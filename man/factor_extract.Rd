% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/favar.R
\name{factor_extract}
\alias{factor_extract}
\title{function \link{fac,lam}=extract(data,k) extracts first k principal components from
t\emph{n matrix data, loadings are normalized so that lam'lam/n=I, fac is t}k, lam is n*k}
\usage{
factor_extract(.data, nfactors = 2)
}
\arguments{
\item{.data}{data from which the factors should be extracted}

\item{nfactors}{number of factors that are going to be extracted}
}
\value{
matrix with the extracted factors

.data = econdata::bbe2005\link{,-1}
}
\description{
function \link{fac,lam}=extract(data,k) extracts first k principal components from
t\emph{n matrix data, loadings are normalized so that lam'lam/n=I, fac is t}k, lam is n*k
}
